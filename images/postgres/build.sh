#!/bin/bash
base=pgsinglelayer
DOCKER_BUILDKIT=0 docker build -f Dockerfile.${base} --squash  -t ${base}:latest .
ID=$(docker images  --filter "reference=${base}:latest" --format "{{json .}}" | jq -r .ID)
docker tag ${ID} localhost:5000/${base}:latest
docker push localhost:5000/${base}:latest
