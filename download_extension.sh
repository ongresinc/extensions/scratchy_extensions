#!/bin/bash

# Some extensions change the internal name to another, so we need to 
# a mechanism to rename the whole thing.
mkdir -p repos/ 2>/dev/null
URL=$1
STRIPEXTPATH="$(echo $1 | sed 's#.*/\([^/]*\)\.git#\1#')"
DEST=${2:-$STRIPEXTPATH}
DESTDIR="repos/$DEST"

git clone $URL $DESTDIR