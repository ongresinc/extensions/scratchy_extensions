#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
ARTIFACTSPATH=${SCRIPTPATH}/artifacts
CSVPATH=${ARTIFACTSPATH}/extensions.csv

cat /dev/null > ${CSVPATH}

for ext in $(find ${ARTIFACTSPATH} -type d -printf "%P\n"); do
    cat ${ARTIFACTSPATH}/${ext}/${ext}.csv >> ${CSVPATH}
done

# Format:
# EXT,NAME,SHA256,UNCOMPRESSED SHA,SIZE