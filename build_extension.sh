#!/bin/bash
#
export PG_VERSION=$1

export EXT=$2
export EXT_BUILD=repos/${EXT}/build
# pushd $EXT


# --rm 
docker rm -f pgxn_${EXT} 2> /dev/null
docker run -d -it --name pgxn_${EXT} --mount "type=bind,src=$(pwd),dst=/repo" pgxn/pgxn-tools:latest \
    bash -c "/repo/_start.sh ${PG_VERSION} ${EXT} && touch /DONE && sleep 1h " 


docker ps 

mkdir -p ${EXT_BUILD} 2> /dev/null

res="$(docker exec  pgxn_${EXT} bash -c 'if [[ -f /DONE ]]; then echo 1; else echo 0 ; fi ')"
while [ $res == "0" ]; do
    sleep 1
    res="$(docker exec  pgxn_${EXT} bash -c 'if [[ -f /DONE ]]; then echo 1; else echo 0 ; fi ')"
done


docker logs pgxn_${EXT}
docker exec pgxn_${EXT} bash -c "touch /usr/share/postgresql/${PG_VERSION}/extension/${EXT}.empty"

for prefix in  pg_ pg ; do
    removedEXTPrefix=${EXT#$prefix}
done
echo ====${removedEXTPrefix}

for file in $(docker exec  pgxn_${EXT} bash -c "ls /usr/share/postgresql/${PG_VERSION}/extension/*${removedEXTPrefix}*"); do 
    # echo "${file}"
    pathtocopy="pgxn_${EXT}:${file}"
    docker  cp  ${pathtocopy}  ${EXT_BUILD}/
done

# docker exec pgxn_${EXT} bash -c "ls /usr/lib/postgresql/${PG_VERSION}/lib/bitcode/${EXT}"

for file in $(docker exec  pgxn_${EXT} bash -c "find /usr/lib/postgresql/${PG_VERSION}/lib -name '*'''${EXT}'''*' ");
do
    pathtocopy="pgxn_${EXT}:${file}"
    docker  cp  ${pathtocopy}  ${EXT_BUILD}/
done

# FIXME! Not all images have bitcodes, so we ignore the errors here. Ideally we should check fo
docker exec pgxn_${EXT} bash -c "mkdir -p /usr/lib/postgresql/${PG_VERSION}/lib/bitcode/${EXT} && touch /usr/lib/postgresql/${PG_VERSION}/lib/bitcode/${EXT}/empty"
docker cp pgxn_${EXT}:/usr/lib/postgresql/${PG_VERSION}/lib/bitcode/${EXT} ${EXT_BUILD}/ 


# popd
