#!/bin/bash

set -xe
export PG_VERSION=${1}
export EXT=${2}


pushd /repo/repos/$EXT
pg-start $PG_VERSION

if [ "$EXT" == "orafce" ]; 
then
apt install -y bison flex
fi

if [ "$EXT" == "oracle_fdw" ]; then
    apt install -y libociei liboci-dev
fi

if [ "$EXT" == "pg_xxhash" ]; then
    apt install -y libxxhash-dev
fi

pg-build-test

popd