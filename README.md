# scratchy_extensions

## Requirements

For running the Dockerfile build, the `experimental` features should be enabled in the 
docker daemon.

```bash
cat > /etc/docker/daemon.json <<EOF
{
"experimental": true
}
EOF

sudo systemctl restart docker.service
```

Also, you need a local registry running over `localhost:5000`.

```bash
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

## Steps

> Version is Major Version (15,13, etc.)

```bash
./download_extension https://github.com/HypoPG/hypopg.git #use HTTPS preferably
./build.sh <version> <EXT>
./pull_artifacts.sh <ext>
./synth_csv.sh 
```

- `generate_artifacts.sh` compiles the extension and places the generated files inside `build/`
folder. The files inside the `build/<ext>`, are the bitcodes.
- Dockerfiles are generated in:
  - Inside the extension folder, under Dockerfile. This builds 2 images, the last one is in charge to aggregate all the layers (due to COPY).
  - Inside /repos, as Dockerfile.extension. This Dockerfile is used by `test.sh`, it spins the official Postgres Docker image and applies the generated extension image.
- synth_csv.sh collects all the CSVs across extensions en artifacts and build a global CSV.
- The `build.sh` is just a wraper of the following scripts:

```bash
./build_extension.sh <version> <EXT>
./build_image.sh <version> <ext>
```

- Some extensions change their internal names. Use `./download_extension.sh https://github.com/pgvector/pgvector.git vector` and vector as the <ext> name.
- Optionally, use test (build executes the test):

```bash
./test_image.sh <ext>
```
- `sync_csv.sh` generates a CSV of the extensions in the `artifacts/` folder with the following fields:

```
EXT,NAME,SHA256,UNCOMPRESSED SHA,SIZE
```

## Starting Postgres manually

Or, manually:

```bash
cd <ext>
docker build . -t <ext>
docker run -it -d  <ext> -e POSTGRES_PASSWORD=mysecretpassword postgres
docker exec -it pgxn_<ext> psql -U postgres -c "CREATE EXTENSION <ext>"
```