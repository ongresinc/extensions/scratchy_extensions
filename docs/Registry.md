# Registry API

```bash
curl -X GET https://localhost:5000/v2/_catalog

curl http://localhost:5000/v2/pg_uuidv7/tags/list
curl http://localhost:5000/v2/pg_uuidv7/blobs/<reference is sha256:xxx>
curl http://localhost:5000/v2/pg_uuidv7/manifests/latest

curl --cacert domain.crt https://your.registry:5000/v2/busybox/tags/list
```

## References
[API reference](https://github.com/distribution/distribution/blob/main/docs/spec/api.md)


