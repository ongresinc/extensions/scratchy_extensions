#!/bin/bash

EXT=$1
# PGVERSION=$2
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
# DF=${SCRIPTPATH}/repos/$EXT/Dockerfile
EXTPATH=${SCRIPTPATH}/repos/$EXT
ARTIFACTSPATH=${SCRIPTPATH}/artifacts/$EXT
# pushd ${EXTPATH}
REPOSPATH=${SCRIPTPATH}/repos

REGISTRY="curl --silent http://localhost:5000/v2/${EXT}/"

MANIFESTS="manifests/latest"

BOOBS="blobs/" # Ups, sorry the typo

LYCSVPATH=${ARTIFACTSPATH}/${EXT}.csv

mkdir -p ${ARTIFACTSPATH} 2> /dev/null
cat /dev/null > ${LYCSVPATH}

for layer in $(${REGISTRY}${MANIFESTS} | jq -r '.fsLayers[].blobSum' );
do  
    sanitize_layer=${layer#*:} #remove the sha256: prefix
    if [ $(${REGISTRY}${BOOBS}${layer} | wc -l)  -eq 0 ]; then #skipping config layer, which has a single block
        continue
    fi
    ${REGISTRY}${BOOBS}${layer} -o ${ARTIFACTSPATH}/${sanitize_layer}.tar.gz
    uncompSHA=$(gunzip -dc ${ARTIFACTSPATH}/${sanitize_layer}.tar.gz | sha256sum - | cut -f1 -d " ") 
    gunzip -dc ${ARTIFACTSPATH}/${sanitize_layer}.tar.gz | tar tf - > ${ARTIFACTSPATH}/${sanitize_layer}.contents
    layerSz=$(du -b -0 ${ARTIFACTSPATH}/${sanitize_layer}.tar.gz | cut -f1)
    echo "${EXT},${sanitize_layer},${uncompSHA},${layerSz}" >> ${LYCSVPATH}
    cp ${REPOSPATH}/Dockerfile.${EXT} ${ARTIFACTSPATH}/
done 

