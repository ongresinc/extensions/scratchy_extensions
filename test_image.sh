#!/bin/bash

EXT=$1

docker rm -f ${EXT} 2> /dev/null
pushd repos
docker build -f Dockerfile.${EXT} -t test_${EXT}:latest .
docker run --rm -d --name ${EXT} -e POSTGRES_PASSWORD=mysecretpassword test_${EXT} postgres

## pg_isready returns ok, but connections aren't allowed yet
# until [ "$(docker exec ${EXT} bash -c 'pg_isready | echo $?')" == "0" ]; do 
# sleep 10s
# done 

sleep 30s
docker exec -it ${EXT} psql -U postgres -c "CREATE EXTENSION \"${EXT}\" CASCADE"
docker rm -f ${EXT}
popd