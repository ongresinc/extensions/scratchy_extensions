#!/bin/bash
## NOTE:
## As we start from scratch, it is not possible to use RUN, as it uses sh underlying.
## So, to avoid one layer per copy, we use a final stage that copies recursively all the
## files in the generated image.


EXT=$2
PGVERSION=$1
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
DF=${SCRIPTPATH}/repos/$EXT/Dockerfile
EXTPATH=${SCRIPTPATH}/repos/$EXT
pushd ${EXTPATH}

echo -e "FROM scratch as pre_${EXT}\n" > $DF
echo >> $DF

pushd build


for file in $(find . -maxdepth 1 -type f -printf "%P\n") ; do
    echo -e "COPY  build/$file /usr/share/postgresql/${PGVERSION}/extension/\n" >> $DF
done    

## Look for the so, control and other bc in the lib
for file in $(ls ${EXT}.* ); do 
    echo -e "COPY  build/$file /usr/lib/postgresql/${PGVERSION}/lib/\n" >> $DF
done 

# echo -e "RUN mkdir -p /usr/lib/postgresql/15/lib/bitcode/${EXT}\n" >> $DF

for file in $(ls $EXT/); do 
    echo -e "COPY build/${EXT}/$file /usr/lib/postgresql/${PGVERSION}/lib/bitcode/${EXT}/\n" >> $DF
done 

cat >> $DF << EOF
FROM scratch as ${EXT}
COPY --from=pre_${EXT} / /

EOF


popd 

docker build . --squash --force-rm --no-cache -t ${EXT}:latest  
ID=$(docker images  --filter "reference=${EXT}:latest" --format "{{json .}}" | jq -r .ID)
docker tag ${ID} localhost:5000/${EXT}:latest
docker push localhost:5000/${EXT}:latest

# Test image build
DF=${SCRIPTPATH}/repos/Dockerfile.${EXT}

echo >$DF
echo -e "FROM postgres:${PGVERSION}-bookworm\n" >> $DF
echo -e "COPY --from=${EXT} /usr/share/postgresql/${PGVERSION}/extension/ /usr/share/postgresql/${PGVERSION}/extension/\n" >> $DF
echo -e "COPY --from=${EXT} /usr/lib/postgresql/${PGVERSION}/lib/ /usr/lib/postgresql/${PGVERSION}/lib/\n" >> $DF

echo -e "COPY --from=${EXT} /usr/lib/postgresql/${PGVERSION}/lib/bitcode/${EXT}/ /usr/lib/postgresql/${PGVERSION}/lib/bitcode/${EXT}/\n" >> $DF

popd 






